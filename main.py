import math

PHI1 = (math.sqrt(5) - 1) / 2  # 1 / phi
PHI2 = (3 - math.sqrt(5)) / 2  # 1 / phi^2
NEW = True


def counter(function):
    def wrapper(x):
        global NEW
        if NEW:
            wrapper.count = 0
        wrapper.count += 1
        NEW = False
        return function(x), wrapper.count
    return wrapper


@counter
def function(x):
    return math.exp(x)*(x**2)


def golden_section_method(f, a, b, eps):
    global NEW
    NEW = True
    n_fact = 0
    n_exp = n = int(math.ceil(math.log(eps / (b-a)) / math.log(PHI1)))
    (a, b) = (min(a, b), max(a, b))
    if b-a <= eps:
        return a, b
    c = a + PHI2 * (b-a)
    d = a + PHI1 * (b-a)
    yc, calls_counter = f(c)
    yd, calls_counter = f(d)

    while b-a > eps:
        n_fact += 1
        if yc < yd:
            b = d
            d = c
            yd = yc
            c = a + PHI2*(b - a)
            yc, calls_counter = f(c)
        else:
            a = c
            c = d
            yc = yd
            d = a + PHI1*(b - a)
            yd, calls_counter = f(d)

    return a + (b - a)/2, n_fact, n_exp, calls_counter, b-a


accuracy = [0.1, 0.01, 0.001]
for eps in accuracy:
    answer, n_fact, n_exp, calls_counter, h = golden_section_method(function, -4, 1, eps)
    print('\neps: ' + str(eps) + '\nAnswer: ' + str(answer) + '\ninterval length: ' + str(h))
    print(str('n_fact: ' + str(n_fact) + '\nn_exp: ' + str(n_exp) +
              '\nfact. function calls: '

+ str(calls_counter)))



